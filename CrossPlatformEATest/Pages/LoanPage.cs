﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using EAAutoFramework.Base;
using EAAutoFramework.Extensions;
using OpenQA.Selenium;

namespace CrossPlatformEATest.Pages
{
    class LoanPage :BasePage
    {
        public LoanPage(ParallelConfig parallelConfig) : base(parallelConfig)
        {
            
        }

        IWebElement SliderElement => _parallelConfig.Driver.FindElementByName("amount");

        IWebElement AmountHeader => _parallelConfig.Driver.FindElementByClassName("loan-amount__header__amount");

        //List<IWebElement> CalenderDate => _parallelConfig.Driver.FindElementsByClassName("date-selector").ToList();

        List<IWebElement> CalenderDate => _parallelConfig.Driver.FindElementsByClassName("date-selector__flex").ToList();

        private IWebElement RepaymentDateElement =>
            _parallelConfig.Driver.FindElementByClassName("loan-schedule__tab__panel__detail__tag__text");


        public string GetLoanValueText()
        {
            return AmountHeader.GetLinkText();

        }

        public void ClickOnSlider()
        {
            SliderElement.ClickWebElement();

        }

        public string GetDayFromSpecificDate(DateTime date)
        {
            {
                var culture = new System.Globalization.CultureInfo("en-GB"); //<- 'es-419' = Spanish (Latin America), 'en-US' = English (United States)
                var ret = culture.DateTimeFormat.GetDayName(date.DayOfWeek);
                ret = culture.TextInfo.ToTitleCase(ret.ToLower()); //<- Convert to Capital title
                return ret;
            }
            
        }

        public void SelectDate(int number)
        {
            for (int i = 1; i < CalenderDate.Count;)
            {
                IWebElement ele = CalenderDate[number];
                ele.Click();
               break;
            }


        }

        public string GetRepaymentDate()
        {
            return RepaymentDateElement.GetLinkText();

        }


    }
}
