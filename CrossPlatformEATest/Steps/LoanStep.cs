﻿using System;
using System.Threading;
using CrossPlatformEATest.Pages;
using EAAutoFramework.Base;
using EAAutoFramework.Config;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace CrossPlatformEATest.Steps
{
    [Binding]
    public class LoanStep: BaseStep
    {
        private readonly ParallelConfig _parallelConfig;

        public LoanStep(ParallelConfig parallelConfig) : base(parallelConfig)
        {
            _parallelConfig = parallelConfig;
        }

        public void NavigateSite()
        {
            _parallelConfig.Driver.Navigate().GoToUrl(Settings.AUT);
            //_parallelConfig.Driver.WaitForPageLoaded();
            Thread.Sleep(TimeSpan.FromSeconds(5));

            //LogHelpers.Write("Opened the browser !!!");
        }

        [Given(@"Naviate to go Loan Page")]
        public void GivenNavigateToGoLoanPage()
        {
            NavigateSite();
            
            _parallelConfig.CurrentPage = new LoanPage(_parallelConfig);

        }

        [Given(@"Page is opened and verify loan value")]
        public void GivenPageIsOpenedAndVerifyLoanValue()
        {
            var defaultLoanValue = _parallelConfig.CurrentPage.As<LoanPage>().GetLoanValueText();
            Assert.That("£200", Is.EqualTo(defaultLoanValue), $"{defaultLoanValue} is not correct");
        }

        [When(@"Click on slider to change the value")]
        public void WhenClickOnSliderToChangeTheValue()
        {
            _parallelConfig.CurrentPage.As<LoanPage>().ClickOnSlider();
        }

        [Then(@"verify the new loan value")]
        public void ThenVerifyTheNewLoanValue()
        {
            var newLoanValue=_parallelConfig.CurrentPage.As<LoanPage>().GetLoanValueText();
            _parallelConfig.CurrentPage.As<LoanPage>().SelectDate(19);
           Assert.That("£200", Is.Not.EqualTo(newLoanValue), $"{newLoanValue} is not changed ");
            
        }


    }
}
