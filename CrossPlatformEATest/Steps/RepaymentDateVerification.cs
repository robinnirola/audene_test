﻿using System;
using System.Globalization;
using System.Threading;
using CrossPlatformEATest.Pages;
using EAAutoFramework.Base;
using EAAutoFramework.Config;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace CrossPlatformEATest.Steps
{
    [Binding]
    class RepaymentDateVerification : BaseStep
    {
        private readonly ParallelConfig _parallelConfig;
        public RepaymentDateVerification(ParallelConfig parallelConfig) : base(parallelConfig)
        {
            _parallelConfig = parallelConfig;
        }

        public void NavigateSite()
        {
            _parallelConfig.Driver.Navigate().GoToUrl(Settings.AUT);
            //_parallelConfig.Driver.WaitForPageLoaded();
            Thread.Sleep(TimeSpan.FromSeconds(5));

            //LogHelpers.Write("Opened the browser !!!");
        }

        [Given(@"Select the WeekendDay from calender")]
        public void GivenSelectTheWeekendDayFromCalender()
        {
            NavigateSite();
            _parallelConfig.CurrentPage = new LoanPage(_parallelConfig);
            
        }

        [Given(@"Check Date is selected")]
        public void GivenCheckDateIsSelected()
        {
            _parallelConfig.CurrentPage.As<LoanPage>().SelectDate(17);
        }

        [When(@"Check Date is on Weekend")]
        public void WhenCheckDateIsOnWeekend()
        {
           var date = _parallelConfig.CurrentPage.As<LoanPage>().GetDayFromSpecificDate(DateTime.Today.AddDays(16));
           Assert.That(date, Is.EqualTo("Saturday"), "Day is not weekend");
        }

        [Then(@"verify the FirstRepayment Date")]
        public void ThenVerifyTheFirstRepaymentDate()
        {
            var dateText = _parallelConfig.CurrentPage.As<LoanPage>().GetRepaymentDate();
            DateTime dt = DateTime.Today.AddDays(15);
            var s = dt.ToString("dddd dd MMM yyyy" ,
                CultureInfo.CreateSpecificCulture("en-US"));
            
            Assert.That(dateText.Contains(s), "Repayment Date is not matching");
        }

    }
}
